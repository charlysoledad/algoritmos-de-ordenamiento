void setup(){  
  float[] arr = { 0.89, 0.73, 0.49, 0.47, 0.23, 0.3, 0.78 };
  print("Arreglo inicial: ");
  for(float i : arr)
    print(i + " ");
    
  bucketSort(arr,7);
    
  print("\nArreglo ordenado: ");
  for(float i : arr)
    print(i + " ");
  
}

void bucketSort(float[] array, int size){

  if(size <= 0) return;
  
  int i = 0;
  ArrayList<Float>[] bucket = new ArrayList[size];
  
  //Creamos los buckets
  for(i = 0; i < size; i++){
    bucket[i] = new ArrayList<Float>();
  }
  
  //Añadir elementos a cada bucket
  int bucket_i = 0;
  for(i = 0; i < size; i++){
    bucket_i = (int) array[i] * size; //Corresponde al bucket de destino
    bucket[bucket_i].add(array[i]);
  }
  
  //Ordenamiento dentro de cada bucket
  for(i = 0; i < size; i++){
    insertionSort(bucket[i]);
  }
  
  //Regresar el arreglo ordenado
  int ind = 0;
  for(i = 0; i < size; i++){
    for(int j = 0, len = bucket[i].size(); j < len; j++){
      array[ind++] = bucket[i].get(j);
    }
  }
}


void insertionSort(ArrayList<Float> array){
  if(array.isEmpty()) return;
  int j;
  float aux = 0;
  for(int i = 0; i < array.size(); i++){
    aux = array.get(i);
    j = i-1;
    while(j >= 0 && array.get(j) > aux){
      array.set(j+1,array.get(j));
      --j;
    }
    array.set(j+1,aux);
  }
}
